clear all;
close all;
clc;
format short  

Fs = 200;   %Frequência de Amostragem
Wp = 100;   %Frequência limite da Banda de Passagem (analógica)
Wr = 400;   %Frequência limite da Banda de rejeição (analógica)

% Atenuações
Amax= 0.5;
Amin= 12;  

%Calculo de E
E = sqrt(10^(0.1*Amax)-1);  

% Digitalização das Frequências Analógicas
Ts = 1 / (Fs);  
wp = Ts * Wp;
wr = Ts * Wr;  

% Pré-distorção das Frequências
Wap = 2*Fs*tan(wp/2);
War = 2*Fs*tan(wr/2); 

% Cálculo da Ordem e da frequência de 3 dB
[n,wc_n] = buttord(Wap,War,Amax,Amin,'s'); 
wo = 1/(E^(1/n)/Wap);      %Determinaçao do filtro normalizado 

% Determina numerador e denominador da H(s) do filtro analógico
[z,p,k]=buttap(n);
b = poly(z)*k;
a = poly(p);  

%Transformaçao do filtro 
[bt,at]=lp2lp(b,a,wo);

%Transformaçao Bilinear 
[NUM,DEN] = bilinear(bt,at,Fs);

hold on
%Plotar Resposta em Frequencia da H(z) obtida pela Transformação Bilinear
w = 0:pi/512:pi; 
H = freqz(NUM,DEN,w); 
plot(w,20*log10(abs(H))) 
grid on  
%Verificar Requisitos 
var = (pi/512);
var1 = ceil(wp/var+1); 
var2 = ceil(wr/var+1);  
%w = 0
mod = 20*log10(abs(H(1)))  
%w = wp
mod = 20*log10(abs(H(var1)))  
%w = wr
mod = 20*log10(abs(H(var2))) 
