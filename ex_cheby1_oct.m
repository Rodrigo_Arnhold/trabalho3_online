clear all;
close all;
clc;
format long g
pkg load signal

Fs = 500;   %Frequencia de amostragem em Hz
Wp = 400;   %Frequencia em rad/s
Wr = 100;   %Frequencia em rad/s

Amax= 0.5;
Amin= 12;

%Calculo de E
E = sqrt(10^(0.1*Amax)-1);

Ts = 1 /(Fs);

wp = Ts * Wp;
wr = Ts * Wr;

Wap = 2*Fs*tan(wp/2);
War = 2*Fs*tan(wr/2);

%Cálculo da ordem do filtro
a = acosh(sqrt((10^(0.1*Amin)-1)/(10^(0.1*Amax)-1)));
b = acosh(Wap/War);
n = ceil(a/b);

wo = Wap;

%Determinaçao do filtro normalizado
[z,p,k]=cheb1ap(n,Amax);

%Transformaçao do filtro
[z, p, k] = sftrans (z, p, k,wo, true);
[bt1, at1] = zp2tf (z, p, k);

%Transformaçao Bilinear
[NUM,DEN] = bilinear(bt1,at1,1/Fs);

% Determina H(z)
h = tf(NUM, DEN, -1, 'variable','z^-1')

%Plotar Resposta em Frequencia
w = 0:0.001/pi:pi;
H = freqz(NUM,DEN,w);
plot(w,abs(H))
grid on
figure
plot(w,angle(H))
grid on
figure
grpdelay(NUM,DEN);  %atraso de grupo
figure
zplane(NUM,DEN);    %polos e zeros
%Verificar Requisitos
var = (0.001/pi);
var1 = ceil(wp/var+1);
var2 = ceil(wr/var+1);
%w = 0
mod = 20*log10(abs(H(1)))
%w = wp
mod = 20*log10(abs(H(var1)))
%w = wr
mod = 20*log10(abs(H(var2)))    