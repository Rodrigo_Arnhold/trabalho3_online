close all;
clear all;
clc;
format short 
%defini��es dos ganhos dos filtros
G1 = 1;
G2 = 1;
G3 = 1;
G4 = 1;


%Le o audio de entrada
[file, Fs] = audioread('mix_musica_mono.wav');
%file - todas as amostras do sinal
%Fs - Taxa de amostras

file = file(:,1);               %considera s� um canal do canal, no caso trabalha com o audio em mono.
time_base = (0:1/Fs:(length(file)-1)/Fs);      %Time base
figure
plot(time_base,file)                  % espectro do audio original
xlim([0 time_base(end)])          % delimita o tamanho do grafico no eixo x
xlabel('Tempo (segundos)')          
ylabel('Amplitude')
title('{\bf Audio}')

m = length(file);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(file,n);       % DFT of signal
f = (0:n-1)*(Fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure
plot(f(1:floor(n)),p(1:floor(n)));   %mostra somente a parte positiva do gr�fico
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio}')
xlim([0 6000])


% Atenua��es
Amax= 0.5;
Amin= 6;  

% frequencia de corte
Fp = 4000;   
Fr = 4500;  
Wp = 2 * pi * Fp;
Wr = 2 * pi * Fr;

% Digitaliza��o das Frequ�ncias Anal�gicas
Ts = 1 / (Fs);  
wp = Ts * Wp;
wr = Ts * Wr;  

% Pr�-distor��o das Frequ�ncias
Wap = 2*Fs*tan(wp/2);
War = 2*Fs*tan(wr/2); 

% C�lculo da Ordem e da frequ�ncia de 3 dB
[n,wc_n] = buttord(Wap,War,Amax,Amin,'s'); 

%Calculo de E
E = sqrt(10^(0.1*Amax)-1);  
wo = 1/(E^(1/n)/Wap);      %Determina�ao do filtro normalizado 

% Determina numerador e denominador da H(s) do filtro anal�gico
[z,p,k]=buttap(n);
b = poly(z)*k;
a = poly(p);  

%Transforma�ao do filtro 
[bt,at]=lp2lp(b,a,wo);

%Transforma�ao Bilinear 
[NUM,DEN] = bilinear(bt,at,Fs);

[H1,W]=freqz(NUM,DEN,Fs);
figure
plot(W/pi,20*log10(abs(H1)));
xlabel('Frequ�ncia (Hz)')
ylabel('Amplitude')
title('{\bf Filtro Butterworth - Passa-Baixas}')

%Aplica o filtro no audio
Filter_G1 = filter(NUM,DEN,file);   %filtragem do audio

m = length(Filter_G1);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(Filter_G1,n);       % DFT of signal
f = (0:n-1)*(Fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure
plot(f(1:floor(n)),p(1:floor(n)));   %mostra somente a parte positiva do gr�fico
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio}')
xlim([0 8000])


% frequencia de corte
Fp1 = 20;   
Fp2 = 100;
Fr1 = 30;  
Fr2 = 80;
Wp1 = 2 * pi * Fp1;
Wp2 = 2 * pi * Fp2;
Wr1 = 2 * pi * Fr1;
Wr2 = 2 * pi * Fr2;

% Digitaliza��o das Frequ�ncias Anal�gicas
wp1 = Ts * Wp1;
wp2 = Ts * Wp2;  
wr1 = Ts * Wr1;
wr2 = Ts * Wr2; 

% Pr�-distor��o das Frequ�ncias
Wap1 = 2*Fs*tan(wp1/2);
Wap2 = 2*Fs*tan(wp2/2); 
War1 = 2*Fs*tan(wr1/2);
War2 = 2*Fs*tan(wr2/2); 

% vetor de frequencia para o passa banda
WAP = [ 20 80 ]/500;
WAR = [ 100 30 ]/500;

[n,wcN] = buttord(WAP,WAR,Amax,Amin); 
[NUM1,DEN1] = butter(n,wcN);

[H2,W]=freqz(NUM1,DEN1,Fs);
figure
plot(W/pi,20*log10(abs(H2)));
xlabel('Frequ�ncia (Hz)')
ylabel('Amplitude')
title('{\bf Filtro Butterworth - Passa-Baixas}')

%Aplica o filtro no audio
Filter_G2 = filter(NUM1,DEN1,file);   %filtragem do audio

m = length(Filter_G2);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(Filter_G2,n);       % DFT of signal
f = (0:n-1)*(Fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure
plot(f(1:floor(n)),p(1:floor(n)));   %mostra somente a parte positiva do gr�fico
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio}')
xlim([0 8000])


audiowrite('exemplo_filtrado.wav',G1*Filter_G1+G2*Filter_G2,Fs);


