clear all
close all
clc



[x,fs] = audioread('mix_musica_mono.wav');

% plot(x(:,1))
% xlabel('N�mero de Amostras')
% ylabel('Amplitude')
% title('{\bf Audio}')

%sound(x,fs)

bCall = x(:,1);
tb = (0:1/fs:(length(bCall)-1)/fs); % Time base

figure
plot(tb,bCall)
xlim([0 tb(end)])
xlabel('Tempo (segundos)')
ylabel('Amplitude')
title('{\bf Audio}')

m = length(bCall);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(bCall,n);       % DFT of signal
f = (0:n-1)*(fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure
plot(f(1:floor(n)),p(1:floor(n)))
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio}')
xlim([0 15000])



%Filtro passa banda na faixa de 340 Hz a 350 Hz************************************
Amax = 0.5;

Amin = 20;

Ts=1/(2*pi*fs);

w1 = 2*pi*70   ;
w2 = 2*pi*142;
w3 = 2*pi*30;
w4 = 2*pi*331;

wo1 = sqrt(w1*w2);

Wp1=[w1 w2];
Ws1=[w3 w4];

b2= w2 - w1;

wp1=Ts*Wp1;                 %normalizacao do filtro analogico
ws1=Ts*Ws1;                 %normalizacao do filtro analogico  

Wap1=2*fs*tan(wp1/2);       %pre-distor��o das frequencias
Was1=2*fs*tan(ws1/2);       %pre-distor��o das frequencias

% c�lculo da ordem 
[n1,wn1]=buttord(wp1,ws1,Amax,Amin,'s');  %calcula a ordem do filtro analogico ( butterworth) 

E = sqrt(10^(0.1*Amax)-1);

E11=E^(1/n1);
B11=b2/E11;

%Determina�ao do filtro normalizado 

[z,p2,k]=buttap(n1); 
b1 = poly(z)*k;              %gerando o numerador
a1 = poly(p2);                %gerando o demominador

%Transforma�ao do filtro

[b1t,a1t]=lp2bp(b1,a1,wo1,b2);  %lp2lp converte PB normaliza p/ PB desnormalizado
                                

%Transforma�ao Bilinear 

[NUM1,DEN1]=bilinear(b1t,a1t,fs);

%plotar resposta em frequencia

w=0:pi/512:pi;
[H1 W]=freqz(NUM1,DEN1,512);

figure
plot(W/pi,20*log10(abs(H1)));
xlabel('Frequ�ncia (Hz)')
ylabel('Amplitude')
title('{\bf Filtro Butterworth - Passa-Banda}')

som=filter(NUM1,DEN1,x(:,1));   %filtragem do audio 

y1 = fft(som,n);       % DFT of signal
f1 = (0:n-1)*(fs/n);  % Frequency range
p1 = y1.*conj(y1)/n;       % Power of the DFT

audiowrite('exemplo_filtrado.wav',som,fs);

figure
plot(f(1:floor(n/2)),p(1:floor(n/2)))
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio}')
xlim([0 2000])

hold on
plot(f1(1:floor(n/2)),p1(1:floor(n/2)),'r--')
hold off

figure
plot(tb,som)
xlabel('Tempo (segundos)')
ylabel('Amplitude')
title('{\bf Audio na Sa�da do Filtro}')