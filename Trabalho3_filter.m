close all;
clear all;
clc;
format short 
%defini��es dos ganhos dos filtros
G1 = 1;
G2 = 1;
G3 = 1;
G4 = 1;


%Le o audio de entrada
[file, Fs] = audioread('mix_musica_mono.wav');
%file - todas as amostras do sinal
%Fs - Taxa de amostras

file = file(:,1);               %considera s� um canal do canal, no caso trabalha com o audio em mono.
time_base = (0:1/Fs:(length(file)-1)/Fs);      %Time base
figure
plot(time_base,file)                  % espectro do audio original
xlim([0 time_base(end)])          % delimita o tamanho do grafico no eixo x
xlabel('Tempo (segundos)')          
ylabel('Amplitude')
title('{\bf Espectro do �udio Original}')

m = length(file);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(file,n);       % DFT of signal
f = (0:n-1)*(Fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure
plot(f(1:floor(n)),p(1:floor(n)));   %mostra somente a parte positiva do gr�fico
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio Original}')
xlim([0 10000])


%--------------------------------------------------------------------
%--------------------------------------------------------------------
%Primeiro filtro(G1) - Passa Baixa - 16db atenua��o
Fpass = 4000;        % Passband Frequency
Fstop = 4500;        % Stopband Frequency
Apass = 0.5;         % Passband Ripple (dB)
Astop = 30;          % Stopband Attenuation (dB)
match = 'stopband';  % Band to match exactly

% Construct an FDESIGN object and call its BUTTER method.
h1  = fdesign.lowpass(Fpass, Fstop, Apass, Astop, Fs);
Hd1 = design(h1, 'butter', 'MatchExactly', match);
Filter_G1 = filter(Hd1,file);                           %Aplica o filtro no audio original

%--------------------------------------------------------------------
%--------------------------------------------------------------------
%Segundo filtro(G2) - Passa Alta  - 13db atenua��o
Fstop = 500;        % Stopband Frequency
Fpass = 700;         % Passband Frequency
Astop = 80;          % Stopband Attenuation (dB)
Apass = 1;           % Passband Ripple (dB)
match = 'passband';  % Band to match exactly

% Construct an FDESIGN object and call its CHEBY1 method.
h2  = fdesign.highpass(Fstop, Fpass, Astop, Apass, Fs);
Hd2 = design(h2, 'cheby1', 'MatchExactly', match);
Filter_G2 = filter(Hd2,Filter_G1);                           %Aplica o filtro no audio original

%--------------------------------------------------------------------
%--------------------------------------------------------------------
%Terceiro filtro(G2) -  - db atenua��o
Fpass1 = 900;         % First Passband Frequency
Fstop1 = 1000;          % First Stopband Frequency
Fstop2 = 1500;          % Second Stopband Frequency
Fpass2 = 1800;         % Second Passband Frequency
Apass1 = 1;           % First Passband Ripple (dB)
Astop  = 28;          % Stopband Attenuation (dB)
Apass2 = 1;           % Second Passband Ripple (dB)
match  = 'stopband';  % Band to match exactly

% Construct an FDESIGN object and call its BUTTER method.
h3  = fdesign.bandstop(Fpass1, Fstop1, Fstop2, Fpass2, Apass1, Astop,Apass2, Fs);
Hd3 = design(h3, 'butter', 'MatchExactly', match);
Filter_G3 = filter(Hd3,Filter_G2);                           %Aplica o filtro no audio original

%--------------------------------------------------------------------
%--------------------------------------------------------------------
%quarto filtro(G4) -  - db atenua��o
Fpass1 = 2000;         % First Passband Frequency
Fstop1 = 2300;         % First Stopband Frequency
Fstop2 = 3800;         % Second Stopband Frequency
Fpass2 = 4100;         % Second Passband Frequency
Apass1 = 1;           % First Passband Ripple (dB)
Astop  = 38;          % Stopband Attenuation (dB)
Apass2 = 1;           % Second Passband Ripple (dB)
match  = 'stopband';  % Band to match exactly

% Construct an FDESIGN object and call its BUTTER method.
h4  = fdesign.bandstop(Fpass1, Fstop1, Fstop2, Fpass2, Apass1, Astop,Apass2, Fs);
Hd4 = design(h4, 'butter', 'MatchExactly', match);
Filter_G4 = filter(Hd4,Filter_G3);                           %Aplica o filtro no audio original




%--------------------------------------------------------------------
%--------------------------------------------------------------------
% Aplica os filtros no audio original
audiowrite('exemplo_filtrado.wav',Filter_G4,Fs);

%L� o arquivo de audio final
[audio_filtrado, Fs] = audioread('exemplo_filtrado.wav');       %read file
time_base = (0:1/Fs:(length(audio_filtrado)-1)/Fs);             %Time base

% FFT do audio final
m = length(audio_filtrado);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(audio_filtrado,n);       % DFT of signal
f = (0:n-1)*(Fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure
plot(f(1:floor(n)),p(1:floor(n)));   %mostra somente a parte positiva do gr�fico
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio Final}')
xlim([0 10000])


% Espectro final do �udio
figure
plot(time_base,audio_filtrado)                  % espectro do audio original
xlim([0 time_base(end)])          % delimita o tamanho do grafico no eixo x
xlabel('Tempo (segundos)')          
ylabel('Amplitude')
title('{\bf Espectro do �udio Final}')


