clear all;
close all;
clc;
format short  

pkg load signal %Carrega pacote de funções para filtragem

Fs = 200;   %Frequência de Amostragem
Wp = 400;   %Frequência limite da Banda de Passagem (analógica)
Wr = 100;   %Frequência limite da Banda de rejeição (analógica)

% Atenuações
Amax= 0.5;
Amin= 12;  

%Calculo de E
E = sqrt(10^(0.1*Amax)-1);  

% Digitalização das Frequências Analógicas
Ts = 1 / (Fs);  
wp = Ts * Wp;
wr = Ts * Wr;  

% Pré-distorção das Frequências
Wap = 2*Fs*tan(wp/2);
War = 2*Fs*tan(wr/2); 

%Calculo da ordem do Filtro
a = log10((10^(0.1*Amin)-1)/(10^(0.1*Amax)-1));
b = 2*log10(Wap/War);
n = ceil(a/b);

wo = (E^(1/n)*Wap);      %Determinaçao do filtro normalizado 

% Determina numerador e denominador da H(s) do filtro analógico
[z,p,k]=buttap(n);

%Transformaçao do filtro
[z, p, k] = sftrans (z, p, k,wo, true);
[bt1, at1] = zp2tf (z, p, k);

%Transformaçao Bilinear 
[NUM,DEN] = bilinear(bt1,at1,1/Fs);

% Determina H(z)
h = tf(NUM, DEN, -1, 'variable','z^-1')

%Plotar Resposta em Frequencia da H(z) obtida pela Transformação Bilinear
w = 0:0.01/pi:pi; 
H = freqz(NUM,DEN,w); 
plot(w,20*log10(abs(H))) 
grid on  
%Verificar Requisitos 
var = (0.01/pi);
var1 = ceil(wp/var+1); 
var2 = ceil(wr/var+1);  
%w = 0
mod = 20*log10(abs(H(1)))  
%w = wp
mod = 20*log10(abs(H(var1)))  
%w = wr
mod = 20*log10(abs(H(var2)))