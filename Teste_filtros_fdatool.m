close all;
clear all;
clc;
format short 

%defini��es dos ganhos dos filtros
G1 = 1;
G2 = 1;
G3 = 1;
G4 = 1;

%Le o audio de entrada
[file, Fs] = audioread('mix_musica_mono.wav');
%file - todas as amostras do sinal
%Fs - Taxa de amostras

file = file(:,1);               %considera s� um canal do canal, no caso trabalha com o audio em mono.
time_base = (0:1/Fs:(length(file)-1)/Fs);      %Time base
figure
plot(time_base,file)                  % espectro do audio original
xlim([0 time_base(end)])          % delimita o tamanho do grafico no eixo x
xlabel('Tempo (segundos)')          
ylabel('Amplitude')
title('{\bf Espectro do Audio Original}')

m = length(file);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(file,n);       % DFT of signal
f = (0:n-1)*(Fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure
plot(f(1:floor(n)),p(1:floor(n)));   %mostra somente a parte positiva do gr�fico
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio Original}')
xlim([0 10000])


%--------------------------------------------------------------------------
%---------------------Filtro G1--------------------------------------------
if G1 == 1                      % valida se o filtro G1 deve ser ativado
    Fstop1 = 0.1;         % First Stopband Frequency
    Fpass1 = 250;         % First Passband Frequency
    Fpass2 = 4800;        % Second Passband Frequency
    Fstop2 = 7000;        % Second Stopband Frequency
    Astop1 = 27;          % First Stopband Attenuation (dB)
    Apass  = 1;           % Passband Ripple (dB)
    Astop2 = 17;          % Second Stopband Attenuation (dB)
    match  = 'passband';  % Band to match exactly

    % Construct an FDESIGN object and call its CHEBY1 method.
    h1  = fdesign.bandpass(Fstop1, Fpass1, Fpass2, Fstop2, Astop1, Apass,Astop2,Fs);
    Hd1 = design(h1, 'cheby1', 'MatchExactly', match);

    % Get the transfer function values.
    [NUM1, DEN1] = tf(Hd1);
else                                %filtro G1 desativado
    NUM1 = 1;              % determina o numerador e denominador igual a 1,
    DEN1 = 1;              % de modo a n�o gerar altera��o no sinal de entrada
end;

%--------------------------------------------------------------------------
%---------------------Filtro G2--------------------------------------------
if G2 == 1                         % valida se o filtro G2 deve ser ativado
    Fpass = 3000;        % Passband Frequency
    Fstop = 3500;        % Stopband Frequency
    Apass = 1;           % Passband Ripple (dB)
    Astop = 25;          % Stopband Attenuation (dB)
    match = 'stopband';  % Band to match exactly

    % Construct an FDESIGN object and call its BUTTER method.
    h2  = fdesign.lowpass(Fpass, Fstop, Apass, Astop, Fs);
    Hd2 = design(h2, 'butter', 'MatchExactly', match);

    % Get the transfer function values.
    [NUM2, DEN2] = tf(Hd2);
else                                %filtro G2 desativado
    NUM2 = 1;              % determina o numerador e denominador igual a 1,
    DEN2 = 1;              % de modo a n�o gerar altera��o no sinal de entrada
end;
%--------------------------------------------------------------------------
%---------------------Filtro G3--------------------------------------------
if G3 == 1                   % valida se o filtro G3 deve ser ativado
Fpass1 = 250;     % First Passband Frequency
Fstop1 = 300;     % First Stopband Frequency
Fstop2 = 400;     % Second Stopband Frequency
Fpass2 = 450;     % Second Passband Frequency
Apass1 = 1;       % First Passband Ripple (dB)
Astop  = 35;      % Stopband Attenuation (dB)
Apass2 = 0.5;     % Second Passband Ripple (dB)
match  = 'both';  % Band to match exactly

% Construct an FDESIGN object and call its ELLIP method.
h3  = fdesign.bandstop(Fpass1, Fstop1, Fstop2, Fpass2, Apass1, Astop,Apass2, Fs);
Hd3 = design(h3, 'ellip', 'MatchExactly', match);

% Get the transfer function values.
[NUM3, DEN3] = tf(Hd3);
else                                %filtro G3 desativado
    NUM3 = 1;              % determina o numerador e denominador igual a 1,
    DEN3 = 1;              % de modo a n�o gerar altera��o no sinal de entrada
end;
%--------------------------------------------------------------------------
%---------------------Filtro G4--------------------------------------------
if G4 == 1                   % valida se o filtro G4 deve ser ativado
Fstop = 500;         % Stopband Frequency
Fpass = 900;         % Passband Frequency
Astop = 60;          % Stopband Attenuation (dB)
Apass = 1;           % Passband Ripple (dB)
match = 'passband';  % Band to match exactly

% Construct an FDESIGN object and call its CHEBY1 method.
h4  = fdesign.highpass(Fstop, Fpass, Astop, Apass, Fs);
Hd4 = design(h4, 'cheby1', 'MatchExactly', match);

% Get the transfer function values.
[NUM4, DEN4] = tf(Hd4);
else                                %filtro G4 desativado
    NUM4 = 1;              % determina o numerador e denominador igual a 1,
    DEN4 = 1;              % de modo a n�o gerar altera��o no sinal de entrada
end;
    
%--------------------------------------------------------------------------

% Gera a resposta em frequencia de cada filtro
[ H1 , W1 ] = freqz(NUM1,DEN1, Fs);
[ H2 , W2 ] = freqz(NUM2,DEN2, Fs);
[ H3 , W3 ] = freqz(NUM3,DEN3, Fs);
[ H4 , W4 ] = freqz(NUM4,DEN4, Fs);
% Plota todas as respostas em uma unica imagem
figure
plot(W1,20*log10(abs(H1)));
hold on
plot(W2,20*log10(abs(H2)),'r');
plot(W3,20*log10(abs(H3)),'k');
plot(W4,20*log10(abs(H4)),'g');
legend('G1 - Passa Banda','G2 - Passa Baixas', 'G3 - Rejeita Banda', 'G4 - Passa Altas')
hold off
xlabel('Frequ�ncia (Hz)')
ylabel('Amplitude')
title('{\bf Resposta em frequ�ncia - Todos os filtros ativos }')

% Converte o numerador e denominador num objeto.
y1 = filter(NUM1,DEN1,file);
y2 = filter(NUM2,DEN2,y1);
y3 = filter(NUM3,DEN3,file);
y4 = filter(NUM4,DEN4,file);

Filtro_total = y1*G1 + y2*G2 + y3*G3 + y4*G4;

%Hdt1 = dfilt.df2(NUM1,DEN1);
%Hdt2 = dfilt.df2(NUM2,DEN2);
%Hdt3 = dfilt.df2(NUM3,DEN3);
%Hdt4 = dfilt.df2(NUM4,DEN4);

% monta o filtro de acordo com o diagrama de bloco do trabalho 3
%ft_g1g2=dfilt.cascade(Hdt1,Hdt2);               %Monta o filtro 1 e 2 em serie
%ft_final=dfilt.parallel(ft_g1g2,Hdt3,Hdt4);    %Monta o paralelo entre o filtro (1+2), 3 e 4
%Filtro_total = filter(ft_final,file);           %aplica o filtro final ao sinal de entrada

% grava o audio com os filtros selecionados
audiowrite('exemplo_filtrado.wav',Filtro_total,Fs);
clear all;                                  % limpa todas as variaveis utilizadas anteriormente

%---------------------------------------------------------------------------
%---------------------------------------------------------------------------

%le o audio final j� com a aplica��o dos filtros
[file, Fs] = audioread('exemplo_filtrado.wav');

time_base = (0:1/Fs:(length(file)-1)/Fs);      %Time base
figure
plot(time_base,file)                  % espectro do audio final
xlim([0 time_base(end)])          % delimita o tamanho do grafico no eixo x
xlabel('Tempo (segundos)')          
ylabel('Amplitude')
title('{\bf Espectro do Audio Final}')

m = length(file);      % Window length
n = pow2(nextpow2(m));  % Transform length
y = fft(file,n);       % DFT of signal
f = (0:n-1)*(Fs/n);  % Frequency range
p = y.*conj(y)/n;       % Power of the DFT

figure                                %plota a fft do audio final
plot(f(1:floor(n)),p(1:floor(n)));   %mostra somente a parte positiva do gr�fico
xlabel('Frequ�ncia (Hz)')
ylabel('Pot�ncia')
title('{\bf Componente de Frequ�ncias de um Audio Final}')
xlim([0 10000])

